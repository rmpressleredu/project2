var markers = [];       //Contains an updated array pointing to each currently displayed marker
var routeLine = null;   //Contains a pointer to the currently drawn route polyline
var map;                //Pointer to the Google Maps Map object
var activeLine;         //Contains an int tracking which line is currently being displayed

/***********************************************************
* void clearMarkers(void);
*
* Removes all marker objects currently displayed on screen
* by calling the setMap(null) function on each Marker
* object in the markers[] array.
************************************************************/

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}

/*********************************************************
* void renderWindow(void);
*
* Draws an infoWindow on the marker that was clicked
* containing the next train time in each
* direction on the current route (activeLine).
*********************************************************/

function renderWindow() {
    $.ajax({
        url: '../php/getTimes.php',
        type: 'post',
        context: this,
        data: {
            route: activeLine,
            station: this.title
        },
        success: function(json) {
            var contentString = "<h4>" + this.title + "</h4>" + "Next train toward "
                + json.endStation
                + ": " 
                + json.nextToEnd 
                + "<br>Next train toward "
                + json.startStation
                + ": "
                + json.nextToStart;
            
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            
            infowindow.open(map,this);
        },
        error: function() {
            alert("ERROR");
        }
    });
}

$(document).ready(function () {
    //Load the map centered on San Fran
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(37.772343, -122.44423),
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(mapCanvas, mapOptions);
    
    //Set highway color to gray so it will not clash with any route color
    map.set('styles', [
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [
                { color: '#D0D0D0' },
                { weight: 1.6 }
            ]
        }
    ]);
  
    //Handle clicks on route names
    $('.routeLink').on('click', function() {
        $.ajax({
            url: '../php/drawRoute.php',
            type: 'post',
            context: this,
            data: {
                routeClicked: $(this).data('routetoend')
            },
            success: function(info) {
                activeLine = $(this).data('routetoend');
            
                //remove existing features on map
                clearMarkers();
                lineLatLng = [];
                if(routeLine != null) {
                    routeLine.setMap(null);
                }
                
                //render markers for stations
                for(var counter = 0; counter < info.length; counter++) {
                    var markerLatLng = new google.maps.LatLng(info[counter].latitude, info[counter].longitude);
                    var marker = new google.maps.Marker({
                        position: markerLatLng,
                        map: map,
                        title: info[counter].stationName
                    });
                    
                    google.maps.event.addListener(marker, 'click', renderWindow);
                    
                    markers.push(marker);
                    
                    lineLatLng.push(markerLatLng);  //This array will contain all markers on the route. To be used in rendering the line.
                }
                
                //render line
                var lineColor = "#" + $(this).data('color');
                
                routeLine = new google.maps.Polyline({
                    path: lineLatLng,
                    geodesic: true,
                    strokeColor: lineColor,
                    strokeOpacity: 1.0,
                    strokeWeight: 5
                });
                
                routeLine.setMap(map);
            },
        });
    });
    
    //Handler to slide away the welcome message.
    $('#dismiss').on('click', function() {
        $(this).parent().slideToggle();
    });
});

google.maps.event.addDomListener(window, 'load', initialize);
