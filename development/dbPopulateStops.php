<?php
    require('../php/includes.php');
    
    //Create DB connection - see includes.php for DB-related variables
    $dbh = new PDO($connectString, $dbUser, $dbPass);
    
    //Get list of route numbers from Routes table
    $routes = preparedQuery($dbh, "SELECT RouteToEnd
                                   FROM Routes");
    
    //Iterate through returned list of Routes                               
    foreach($routes as $route) {
    
        //Get the list of all stations on this route
        $stopsXML = new SimpleXMLElement("http://api.bart.gov/api/route.aspx?cmd=routeinfo&route={$route['RouteToEnd']}&key=$bartKey", 0, TRUE);
        
        //For each station (children of <config>), insert row in Stops table with RouteToEnd as the first field, 
        //and the Station abbreviation as the second
        foreach($stopsXML->routes->route->config->children() as $stop) {
            $insert = "INSERT INTO Stops
                       VALUES (?, ?)";
                       
            $data = array($route['RouteToEnd'], (string)$stop);
            
            preparedQuery($dbh, $insert, $data);
        }
    }
?>
