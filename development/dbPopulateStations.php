<?php
    require('../php/includes.php');
    
    //Create DB connection - see includes.php for DB-related variables
    $dbh = new PDO($connectString, $dbUser, $dbPass);
    
    //Pulls the list of stations from BART API and stores in SimpleXMLElement $routeXML
    $routeXML = new SimpleXMLElement("http://api.bart.gov/api/stn.aspx?cmd=stns&key=$bartKey", 0, TRUE);
    
    foreach($routeXML->stations->children() as $item) {
        
        //Build query to insert current $item into the Stations table
        $insert = "INSERT INTO Stations (StationAbbr, StationName, Latitude, Longitude) 
                   VALUES (?, ?, ?, ?)";
        $data = array((string)$item->abbr, 
                      (string)$item->name, 
                      (double)$item->gtfs_latitude, 
                      (double)$item->gtfs_longitude);
        
        //Print query to screen for debugging purposes
        echo "Executing SQL statement: $insert <br>";
        
        //Execute query
        preparedQuery($dbh, $insert, $data);
    }
?>
