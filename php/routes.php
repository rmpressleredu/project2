<?php
    require('includes.php');
    
    //Connect to database (see includes.php) and get the entire Routes table.
    $dbh = dbConnect();
    $routes = preparedQuery($dbh, "SELECT * FROM Routes");
    
    //Build an HTML block for displaying each station and append it to array $routeNames
    foreach($routes as $route) {
        //Use SELECT statement to get the name of the beginning station for the current route
        $select = "SELECT StationName 
                   FROM Stations 
                   WHERE StationAbbr='{$route['RouteStart']}'";
        $start = preparedQuery($dbh, $select);
        
        //Use SELECT statement to get the name of the ending station for the current route
        $select = "SELECT StationName 
                   FROM Stations 
                   WHERE StationAbbr='{$route['RouteEnd']}'";
        $end = preparedQuery($dbh, $select);
        
        //Build the HTML block
        $routeNames[] = "<span class='routeColor' style='background-color: #{$route['Color']}'>
                          &nbsp;&nbsp;</span>
                          <a class='routeLink' href='#' data-RouteToEnd='{$route['RouteToEnd']}' data-Color='{$route['Color']}'>"
                        . $start[0]['StationName']
                        . " <=> "
                        . $end[0]['StationName']
                        . "</a>";
    }
    
    //Build the routeList Panel and send it back to the HTML document.
    echo "<div id='routeList'>" 
         . implode("<br>", $routeNames) . "</div>";
?>
