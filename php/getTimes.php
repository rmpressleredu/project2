<?php
    require('../php/includes.php');
    header('content-type:application/json');
    date_default_timezone_set('America/Los_Angeles');

    /********************************************************************************
    * string nextTrainTime($routeID, $stationAbbr, $bartKey)
    *
    * Returns the next time a train will pass through station ($stationAbbr)
    * along route ($routeID) as a string.
    *
    * many $routeID = Integer or string containing integer for the RouteID desired
    * string $stationAbby = 4-character abbreviation for the station
    * string $bartKey = string contained in includes.php to be provided to the API
    *********************************************************************************/
    
    function nextTrainTime($routeID, $stationAbbr, $bartKey) {
        //Get current time in the format xx:xx AM/PM
        $currentTime = date('h:i A');
        
        //Retrieve station schedule
        $schedXML = new SimpleXMLElement(
                            "http://api.bart.gov/api/sched.aspx?cmd=routesched&route=$routeID&key=$bartKey",
                            0, 
                            true);
        
        //Find which stop number this station is on this route
        $stopIndex = 0;
        foreach ($schedXML->route->train[0]->children() as $stop) {
            $thisStation = $stop->attributes();
            if ($thisStation['station'] == $stationAbbr) {
                //If this first arrival happens to be the next train, return its time
                if (strtotime($thisStation['origTime']) > strtotime($currentTime)) {
                    return $thisStation['origTime'];
                }
                break;
            }
            else {
                $stopIndex++;
            }
        }
        
        //Iterate through remaining arrivals/departures for the day, searching for next time
        foreach ($schedXML->route->children() as $train) {
            $thisTrain = $train->attributes();
            if ($thisTrain['index'] == "1") {
                continue;
            }
            
            $thisStop = $train->stop[$stopIndex]->attributes();
            
            //Check to see if time is after now. If so, return this time.
            if (strtotime($thisStop['origTime']) > strtotime($currentTime)) {
                return $thisStop['origTime'];
            }
        }
    }
    
    $routeToEnd = $_POST['route'];
    $station = $_POST['station'];
    
    /*$routeToEnd = 1;
    $station = "West Oakland";*/
    
    //connect to DB
    $dbh = dbConnect();
    
    //Get station abbreviation
    $stationData = preparedQuery($dbh, "SELECT StationAbbr FROM Stations WHERE StationName=?", array($station));
    $stationAbbr = $stationData[0]['StationAbbr'];
    
    //Get other route number, as well as start and end stations
    $routeData = preparedQuery($dbh, "SELECT RouteToStart, RouteStart, RouteEnd FROM Routes WHERE RouteToEnd=?", array($routeToEnd));
    $routeToStart = $routeData[0]['RouteToStart'];
    $routeStart = $routeData[0]['RouteStart'];
    $routeEnd = $routeData[0]['RouteEnd'];
    
    //Get full names of start and end stations
    $endData = preparedQuery($dbh, "SELECT StationName FROM Stations WHERE StationAbbr=? OR StationAbbr=?", array($routeEnd, $routeStart));
    $routeEnd = $endData[0]['StationName'];
    $routeStart = $endData[1]['StationName'];
    
    //Get next train time for trains in both directions
    $trainToEnd = nextTrainTime($routeToEnd, $stationAbbr, $bartKey);
    $trainToStart = nextTrainTime($routeToStart, $stationAbbr, $bartKey);
    
    echo json_encode(array("nextToEnd" => (string)$trainToEnd, 
                           "nextToStart" => (string)$trainToStart, 
                           "endStation" => $routeStart,
                           "startStation" => $routeEnd));
?>
