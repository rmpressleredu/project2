<?php
    //Database variables
    define("DB_HOST", "localhost");
    define("DB_NAME", "rp_Bart");
    define("DB_USER", "rmpressler");
    define("DB_PASS", "harvard.cs75");
    
    $bartKey = "MW9S-E7SL-26DU-VV8V";
    
    /*********************************************************
    * PDO dbConnect(void);
    * 
    * Returns a PHP Data Object connected to the MySQL
    * database specified in the DB constants in includes.php
    *********************************************************/

    function dbConnect() {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";port=3306;dbname=" . DB_NAME, DB_USER, DB_PASS);
        return $dbh;
    }
    
    /********************************************************
    * array(array) preparedQuery($dbh, 
    *                            $statement, 
    *                            $data = []);
    * 
    * Executes a $statement with $data used to fill in the 
    * placeholding ?'s to the database in $dbh. Used to 
    * escape all SQL input
    *
    * Returns a two-dimensional array containing the result set
    * 
    * PDO $dbh = holds a reference to the PDO handle
    * string $statement = holds the query string
    * $data[] = holds a value for each ? in $statement.
    *           Empty by default.
    *********************************************************/                             
    
    function preparedQuery($dbh, $statement, $data = []) {
        $query = $dbh->prepare($statement);
        $query->execute($data);
        $queryArray = $query->fetchAll(PDO::FETCH_ASSOC);
        return $queryArray;
    }
?>
