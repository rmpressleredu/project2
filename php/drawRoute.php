<?php
    require('../php/includes.php');
    header("content-type:application/json");
    
    //Create DB connection - see includes.php for DB-related variables
    $dbh = dbConnect();
   
    //Acquire station information for every staion on the route
    $select = "SELECT StationAbbr, StationName, Latitude, Longitude
               FROM Stops
                    NATURAL JOIN Stations
               WHERE RouteToEnd=?";
    $routeInput = array($_POST['routeClicked']);
    $stationList = preparedQuery($dbh, $select, $routeInput);
    
    //Build the JSON object containing the relevant info about each station
    //from the database.
    foreach($stationList as $station) {
        $stationJSON[] = array('stationName' => $station['StationName'],
                               'latitude' => (double)$station['Latitude'],
                               'longitude' => (double)$station['Longitude']);
    }
    
    echo json_encode($stationJSON);
?>
