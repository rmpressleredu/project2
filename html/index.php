<!DOCTYPE html>
<html>
<head>
  <link rel='stylesheet' type='text/css' href='../css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='../css/main.css'>
  <script src='../js/jquery-1.11.3.min.js'></script>
  <script src="https://maps.googleapis.com/maps/api/js"></script>
  <script src='../js/application.js'></script>
</head>
<body>
  <div id='map-canvas'>
  </div>
  <div id='map-action'>
    <div class="panel panel-default">
     <div class="panel-heading">
        <h3 class="panel-title">
           Choose a Route
        </h3>
     </div>
     <div class="panel-body">
        <?php require('../php/routes.php'); ?>
     </div>
    </div>
  </div>
  <div id='map-welcome'>
    <h1>Welcome to the BART!</h1>
    <h2>Bay Area Rapid Transit</h2>
    <p>To get started, click on a route in the above panel. When the route appears on the map, 
    click any marker to see when the next train will arrive in each direction. All times are in Pacific time
    as the BART is San Francisco-based.</p>
    <p>Project by Richard Pressler</p>
    
    <a href='#' id='dismiss'>Click here to dismiss this message and get started!</a>
  </div>
</body> 
</html>
