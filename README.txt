Richard Pressler
richard@richardpressler.com OR rmpressler@gmail.com
Goffstown, NH - based developer
Completed current version 7/8/2015

This website was a project assigned to students of Harvard's CS75 course: Creating Dynamic Websites. The assignment was to create a 
website that allows users to view the routes on the Bay Area Rapid Transit subway in San Francisco, CA using an interactive Google Map. It was the first time JavaScript had been introduced in the course.
Project specifications: http://cdn.cs75.net/2012/summer/projects/2/project2.pdf

Technologies required to be used were:
-PHP
 -PDO (optionally)
-MySQL
-HTML/CSS
-JavaScript
 -AJAX calls
 -jQuery (optionally)
-git version control

I also chose to use the following:
-Bootstrap CSS to speed up development of front-end

Development environment:
-Fedora 22
-gedit
-Apache
-MariaDB
-Chrome (linux and Windows), Firefox (linux and Windows), IE10 (Windows) for testing

Site File Structure:

/css
    [various Bootstrap CSS files]
    main.css
/development - This folder contains scripts used to aid in development that are not part of the application itself
    dbPopulateStations.php - Obtains current list of stations from bart.gov and adds them to the Stations table
    dbPopulateStops.php - Builds the Stops table, linking Routes to Stations based on data retrieved from bart.gov
/fonts
    [various Bootstrap CSS files]
/html
    index.php - main controller
/js
    [various Bootstrap CSS files]
    application.js
/php
    drawRoute.php - Handles building the station markers to be sent to application.js after an AJAX call
    getTimes.php - Handles generation of train times when a marker is clicked on the map
    includes.php - various helper functions and global variables
    routes.php - Handles generating the HTML to be used to display the route list overlaying the map
